window.addEventListener("load", async function() {
	var logo = document.getElementById('page_head-logo');

	var home = document.querySelector('meta[name="home"]').content;
	var home_href = 'index.php/module/' + home;

	var modulefullname = document.querySelector('meta[name="modulefullname"]').content;

	if (modulefullname!=home) {
		logo.style.cursor = 'pointer';
		logo.addEventListener('click', function() {
			location.href = home_href;
		});
	}
	
});